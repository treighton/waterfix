<?php
/**
Template Name: Page Builder Template
 */

//the_post();

$context = Timber::get_context();
$post = Timber::query_post();
$context['post'] = $post;

Timber::render( 'page-builder.twig', $context );

