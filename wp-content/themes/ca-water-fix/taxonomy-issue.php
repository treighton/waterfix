<?php

$templates = 'archive-issue.twig';

$context = Timber::get_context();

$params = Timber\URLHelper::get_params();

$context['term_page'] = new TimberTerm($params['1']);
$context['title'] = $context['term_page']->name;

$context['issues'] = get_terms('issue');
$context['in_issue'] = Timber::get_posts(
    array(
        'post_type' => 'newsletters',
        'tax_query' => array(
            array(
                'taxonomy' => 'issue',
                'field' => 'slug',
                'terms' => $context['term_page']->slug,
            ),
        ),
    )
);

$context['posts'] = Timber::get_posts(array(
    'tax_query' => array(
        array(
            'taxonomy' => 'issue',
            'terms' => $params['1'],
        ),
    ),
));

Timber::render($templates, $context);
