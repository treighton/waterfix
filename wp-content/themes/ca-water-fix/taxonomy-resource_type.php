<?php

function cmp($a, $b)
{
	if ($a == $b) {
		return 0;
	}
	return ($a < $b) ? -1 : 1;
}

function get_kiddo_taxes($queried_object){
	$termKiddos = get_term_children( $queried_object->term_id, $queried_object->taxonomy );
	$results = array();
	foreach ( $termKiddos as $child ) {
		$term = get_term_by( 'id', $child, $queried_object->taxonomy);
		$results[$term->name] = array(
			'posts' => array(),
			'title' => $term->name,
			'link'  => $term->slug,
			'count' => null
		);
		$args = array(
			'post_type' => 'resources',
			'order' => 'DESC',
			'order_by' => 'date',
			'resource_type' => $term -> slug,
			'posts_per_page' => 3

		);
		$argsCount = array(
			'post_type' => 'resources',
			'order' => 'DESC',
			'order_by' => 'date',
			'resource_type' => $term -> slug,
			'posts_per_page' => -1

		);
		$results[$term->name]['posts'] = Timber::get_posts($args);
		$results[$term->name]['count'] = count(Timber::get_posts($argsCount));
	}
	return $results;
}

function get_child_terms($params, $taxonomy){
	$results = array();
	$term = get_term_by( 'slug', $params, $taxonomy);
	$term_children = get_term_children( $term->term_id, $taxonomy );
	foreach ($term_children as $child) {
		$term = get_term_by( 'id', $child, $taxonomy);
		$results[] = $term;
	}
	return $results;
}

function get_parent($queried_object) {
	$parent = get_term_by('id', $queried_object->parent, $queried_object->taxonomy);
	return $parent;
}

function get_siblings($queried_object) {
	$parent = get_parent($queried_object);
	$siblings = get_term_children( $parent->term_id, $parent->taxonomy );
	return $siblings;
}

function sibling_list($queried_object) {
	$sibling_list = array();
	$siblings = get_siblings($queried_object);
	foreach ($siblings as $sibling) {
		$term = get_term_by('id', $sibling, $queried_object->taxonomy);
		$sibling_list[$term->name] = array(
			'name' => $term->name,
			'link' => $term->slug
		);
	}
	return $sibling_list;
}

$templates = 'archive-resource_type.twig';

$context = Timber::get_context();

$queried_object = get_queried_object();

$context['title'] = single_cat_title( '', false );

$context['parent'] = get_parent($queried_object);

$context['children'] = get_kiddo_taxes($queried_object);

if ($context['parent']) {
	$context['siblingList'] = sibling_list($queried_object);
}

$context['posts'] = Timber::get_posts();

$context['term_page'] = new TimberTerm();

Timber::render( $templates, $context );



