<?php

if (!class_exists('Timber')) {
    add_action('admin_notices', function () {
        echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url(admin_url('plugins.php#timber')) . '">' . esc_url(admin_url('plugins.php')) . '</a></p></div>';
    });

    add_filter('template_include', function ($template) {
        return get_stylesheet_directory() . '/static/no-timber.html';
    });

    return;
}

Timber::$dirname = array('templates', 'views');

class StarterSite extends TimberSite
{

    public function __construct()
    {

        add_theme_support('post-formats');
        add_theme_support('post-thumbnails');
        add_theme_support('menus');
        add_theme_support('html5', array('comment-list', 'comment-form', 'search-form', 'gallery', 'caption'));
        add_filter('timber_context', array($this, 'add_to_context'));
        add_filter('get_twig', array($this, 'add_to_twig'));
        add_action('pre_get_posts', array($this, 'show_all_posts'));
        add_action('wp_enqueue_scripts', array($this, 'load_theme_files'));
        add_filter('mce_buttons_2', array($this, 'my_mce_buttons_2'));
        add_filter('tiny_mce_before_init', array($this, 'add_custom_styles_to_editor'));
        add_action('init', array($this, 'add_editor_styles'));
        add_action('acf/init', array($this, 'add_acf_functions'));
        add_filter('manage_board_meeting_posts_columns', array($this, 'cwf_columns_head'));
        add_action('manage_board_meeting_posts_custom_column', array($this, 'cwf_columns_content'), 10, 2);

        parent::__construct();
    }

    // ADD NEW COLUMN
    public function cwf_columns_head($defaults)
    {
        $defaults['meeting_date'] = 'Meeting Date';
        $defaults['meeting_location'] = 'Meeting Location';
        return $defaults;
    }

// SHOW THE FEATURED IMAGE
    public function cwf_columns_content($column_name, $post_ID)
    {
        if ($column_name == 'meeting_date') {
            $meet_date = strtotime(get_post_field('date', $postID));
            if ($meet_date) {
                echo date("Y-m-d", $meet_date);
            }
        }
        if ($column_name == 'meeting_location') {
            $addy = get_post_field('address', $postID);
            // $addy = $addy['address'];
            if ($addy) {
                echo $addy['address'];
            }
        }
    }

    public function add_acf_functions()
    {
        acf_add_options_page(array(
            'page_title' => 'Theme General Settings',
            'menu_title' => 'Theme Settings',
            'menu_slug' => 'theme-general-settings',
        ));
    }

    public function add_custom_img_size()
    {
        add_image_size('newsletter', 344.39, 289, array('left', 'bottom'));
    }

    public function load_theme_files()
    {
        $date = new DateTime();

        $theme = wp_get_theme();
        //Change Version Number manually as needed to bust cache issues
        wp_enqueue_style('theme-styles', get_template_directory_uri() . '/public/build/css/style.css', null, $date->getTimestamp(), 'all');
        wp_enqueue_script('theme-scripts', get_template_directory_uri() . '/public/build/js/app.js', null, null, true);
        wp_deregister_script('jquery');
        wp_register_script('jquery', "https://code.jquery.com/jquery-2.2.4.min.js", false, null);
        if (!is_admin()) {
            wp_enqueue_script('jquery');
        }
    }

    public function get_icon($file)
    {
        $theme_url = get_theme_file_path();
        $icon = file_get_contents($theme_url . '/public/build/_images/' . $file . '.svg');
        return $icon;
    }

    public function get_image_from_server($file)
    {
        $theme_url = get_theme_file_uri();
        $image = '<img src="' . $theme_url . '/public/build/_images/' . $file . '" />';
        return $image;
    }

    public function add_to_context($context)
    {
        $context['foo'] = 'bar';
        $context['stuff'] = 'I am a value set in your functions.php file';
        $context['notes'] = 'These values are available everytime you call Timber::get_context();';
        $context['event_closed'] = get_field('registration', 'option');
        $context['notification_bar_text'] = get_field('notification_bar_text', 'option');
        $context['pop_title'] = get_field('title', 'option');
        $context['pop_subtitle'] = get_field('sub_title', 'option');
        $context['pop_body'] = get_field('body', 'option');
        $context['pop_button'] = get_field('button_text', 'option');
        $context['pop_link'] = get_field('link', 'option');
        $context['menu'] = new TimberMenu();
        $context['site'] = $this;
        $context['year'] = date('Y');
        return $context;
    }

    // Callback function to insert 'styleselect' into the $buttons array
    public function my_mce_buttons_2($buttons)
    {
        array_unshift($buttons, 'styleselect');
        return $buttons;
    }

    public function add_custom_styles_to_editor($init_array)
    {
        // Define the style_formats array
        $style_formats = array(
            // Each array child is a format with it's own settings
            array(
                'title' => 'Download Link',
                'selector' => 'a',
                'classes' => 'download',
            ),
            array(
                'title' => 'Button Outline',
                'selector' => 'a',
                'classes' => 'button-outline',
            ),
            array(
                'title' => 'Button Primary',
                'selector' => 'a',
                'classes' => 'button-default',
            ),
            array(
                'title' => 'Blue Text',
                'selector' => 'p',
                'classes' => 'text-blue',
            ),
            array(
                'title' => 'Callout right',
                'selector' => 'blockquote',
                'classes' => 'callout right',
            ),
            array(
                'title' => 'Callout left',
                'selector' => 'blockquote',
                'classes' => 'callout left',
            ),

        );
        // Insert the array, JSON ENCODED, into 'style_formats'
        $init_array['style_formats'] = json_encode($style_formats);

        return $init_array;

    }

    public function add_editor_styles()
    {
        add_editor_style('editor-style.css');
    }

    public function do_gform($form)
    {
        //gravity_form($form['id'], false, false, false, '', true, 1);

        $sc = '[gravityform id="' . $form['id'] . '" title="false" description="false" ajax="true"]';

        echo do_shortcode($sc);

    }

    public function hide_on($date)
    {
        $date = new DateTime($date);
        $now = new DateTime('now', new DateTimeZone('America/Los_Angeles'));

        if ($now > $date) {
            return true;
        }

        return false;

    }

    public function do_form_static($id)
    {
        gravity_form($id, false, false, false, '', true, 1);
    }

    public function gform_scripts($id)
    {
        gravity_form_enqueue_scripts($id, true);
    }

    public function dump($var)
    {
        var_dump($var);
    }

    public function get_perm($id)
    {
        $link = get_permalink($id);
        return $link;
    }

    public function add_to_twig($twig)
    {
        /* this is where you can add your own functions to twig */
        $twig->addExtension(new Twig_Extension_StringLoader());
        $twig->addFunction(new Timber\Twig_Function('get_icon', array($this, 'get_icon')));
        $twig->addFunction(new Timber\Twig_Function('form', array($this, 'do_gform')));
        $twig->addFunction(new Timber\Twig_Function('gform_scripts', array($this, 'gform_scripts')));
        $twig->addFunction(new Timber\Twig_Function('do_form_static', array($this, 'do_form_static')));
        $twig->addFunction(new Timber\Twig_Function('dump', array($this, 'dump')));
        $twig->addFunction(new Timber\Twig_Function('hide_on', array($this, 'hide_on')));
        $twig->addFunction(new Timber\Twig_Function('get_perm', array($this, 'get_perm')));
        $twig->addFunction(new Timber\Twig_Function('get_image_from_server', array($this, 'get_image_from_server')));
        return $twig;
    }

    public function show_all_posts($query)
    {
        if (is_admin()) {
            return;
        }

        if ($query->is_main_query()) {
            $query->set('posts_per_page', -1);
        }
    }
}

new StarterSite();

Routes::map('resources/planning-process', function ($params) {
    $query = 'posts_per_page=3&post_type=resources';
    Routes::load('planning.php', null, null, 200);
});

Routes::map('resources/planning-process/:cat', function ($params) {
    Routes::load('planning.php', $params, null, 200);
});
