$(document).ready(function(){
	// NEWSLETTER SECTION in SOCIAL MEDIA SECTION
	$('.newsletter-icon').on('click', function(){
		$(this).find('.glyphicon-chevron-down').animate({
			'transform'	: 'rotate(180deg)'
		},500);
		
		if ($('.expandable').hasClass('open')) {
			$('.expandable').removeClass('open').animate({
				'height'	: '0'
			},500);
		} else {
			$('.expandable').addClass('open').animate({
				'height'	: '400px'
			},500);
		}
	});
    // PROCUREMENT UPDATES SECTION in BUILDING WATERFIX SECTION
	$('.procurement-updates').on('click', function(){
		$(this).find('.glyphicon-chevron-down-2').animate({
			'transform'	: 'rotate(180deg)'
		},500);
		
		if ($('.expandable2').hasClass('open')) {
			$('.expandable2').removeClass('open').animate({
				'height'	: '0'
			},500);
		} else {
			$('.expandable2').addClass('open').animate({
				'height'	: '350px'
			},500);
		}
	});
	// SEARCH BAR
	$('#search-form .fa').on('click', function(){
		if ($(this).hasClass('fa-search')) {
			$(this).removeClass('fa-search');
			$(this).addClass('fa-close');
			$(this).closest('#search-form').find('#tipue_search_input').css({'padding':'12px'}).animate({
				'width' : '335px'
			},300);
		} else {
			$(this).removeClass('fa-close');
			$(this).addClass('fa-search');
			$(this).closest('#search-form').find('#tipue_search_input').css({'padding':'0'}).animate({
				'width' : '0'
			},300);
		}
	});
    // CLICK HERE FOR MORE DETAILS - BUTTON
    $('#solution .solution-details-remove-hide').on('click', function(){
		if ($(this).closest('.solution-details').find('.solution-details-hide').hasClass('hide')) {
			$(this).closest('.solution-details').find('.solution-details-hide').removeClass('hide');
		} else {
			$(this).closest('.solution-details').find('.solution-details-hide').addClass('hide');
		}
		
		// $(this).closest('.solution-details').find('.solution-details-hide').removeClass('hide');
	});
	$('#solution .return-hide').on('click', function(){
		$(this).closest('.solution-details').find('.solution-details-hide').addClass('hide');
	});
});

$(window).load(function() {
	// PRELOAD
	$(".se-pre-con").fadeOut(1000);
	// FADEIN
	$('.fadeIn').delay(1000).animate({
		'opacity'			: 1
	},800);
	// ANIMATE IN ON WINDOW SCROLL
	(function($) {
		$.fn.visible = function(partial) {
		      var $t            = $(this),
		          $w            = $(window),
		          viewTop       = $w.scrollTop(),
		          viewBottom    = viewTop + $w.height(),
		          _top          = $t.offset().top,
		          _bottom       = _top + $t.height(),
		          compareTop    = partial === true ? _bottom : _top,
		          compareBottom = partial === true ? _top : _bottom;
		    return ((compareBottom <= viewBottom) && (compareTop >= viewTop));
		  };
		})(jQuery);
		$(window).on('scroll',function(event) {
		  $(".animate-in1").each(function(i, el) {
		    var el = $(el);
		    if (el.visible(true)) {
		      el.addClass("scale-in1");
		    } //else {
		      //el.removeClass("come-in");
		    //}
		});
	});
	(function($) {
		$.fn.visible2 = function(partial) {
		      var $t            = $(this),
		          $w            = $(window),
		          viewTop       = $w.scrollTop(),
		          viewBottom    = viewTop + $w.height(),
		          _top          = $t.offset().top,
		          _bottom       = _top + $t.height(),
		          compareTop    = partial === true ? _bottom : _top,
		          compareBottom = partial === true ? _top : _bottom;
		    return ((compareBottom <= viewBottom) && (compareTop >= viewTop));
		  };
		})(jQuery);
		$(window).on('scroll',function(event) {
		  $("#connect .social-icons img").each(function(i, el) {
		    var el = $(el);
		    if (el.visible2(true)) {
		      el.addClass("bounce-in2");
		    } //else {
		      //el.removeClass("come-in");
		    //}
		});
	});
});