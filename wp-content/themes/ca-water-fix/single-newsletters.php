<?php
/**
 * The Template for displaying all single posts
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

$context = Timber::get_context();
$post = Timber::query_post();
$context['post'] = $post;
$terms = wp_get_post_terms( $post->ID, 'issue' );
$term = $terms['0'];
$context['issues']= get_terms('issue');
$context['in_issue'] = Timber::get_posts(
				array(
					'post_type' => 'newsletters',
					'tax_query' => array(
						array(
							'taxonomy' => 'issue',
							'field' => 'slug',
							'terms' => $term->slug
						)
					)
				)
			);

Timber::render( 'single-newsletters.twig', $context );

