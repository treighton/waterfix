<?php
/**
Template Name: Landing Page
 */

$context = Timber::get_context();
$post = Timber::query_post();
$context['post'] = $post;
Timber::render( 'page-landing.twig', $context );
