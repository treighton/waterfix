<?php
/**
Template Name: Construction
 */


function get_status_updates($posts_to_show){
	$args = array(
		'post_type' => 'updates',
		'posts_per_page' => $posts_to_show
	);
	return Timber::get_posts($args);
}

$context = Timber::get_context();
$post = Timber::query_post();
$context['post'] = $post;
$context['updates'] = get_status_updates(5);
Timber::render( 'construction.twig', $context );
