<?php
/**
Template Name: Two Column Page
 */

$context = Timber::get_context();
$post = Timber::query_post();
$context['post'] = $post;
Timber::render( 'page-2col.twig', $context );
