<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.2
 */

/**
 * Get individual issues
 * Get Posts from each issue
 * Return array of issues with newsletters
 */

function get_newsletters_by_issue() {
	$issues = [];

	$terms = Timber::get_terms(
		array(
			'taxonomy' => 'issue'
		)
	);

	foreach ($terms as $term){
		
		$posts = Timber::get_posts(
			array(
				'post_type' => 'newsletters',
				'tax_query' => array(
					array(
						'taxonomy' => 'issue',
						'field' => 'term_id',
						'terms' => $term->term_id
					)
				)
			)
		);

		$issues[$term->name] = [
			'issue' => $term->name,
			'issue_image' => get_field('tax_image', 'term_'.$term->term_id),
			'issue_link' => get_term_link($term->term_id, 'issue'),
			'posts' => $posts
		];
	}

	return $issues;

}

$templates = array( 'archive-news_letters.twig', 'index.twig' );
$context = Timber::get_context();
$context['title'] = post_type_archive_title( '', false );
$context['posts'] = Timber::get_posts();
$context['issues'] = get_newsletters_by_issue();

//var_dump($context['issues']);

Timber::render( $templates, $context );
