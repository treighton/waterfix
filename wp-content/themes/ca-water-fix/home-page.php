<?php
/**
Template Name: home
 */

$context = Timber::get_context();
$post = Timber::query_post();

function get_news_items() {

	$args = array(
		'post_type' => 'resources',
		'resource_type' => 'news-archive',
		'posts_per_page' => 3
	);

	return Timber::get_posts($args);
}


$context['post'] = $post;
$context['news'] = get_news_items();
if ( post_password_required( $post->ID ) ) {
	Timber::render( 'single-password.twig', $context );
} else {
	Timber::render( 'front.twig', $context );
}