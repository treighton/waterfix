<?php
/**
Template Name: Form Page
 */

$context = Timber::get_context();
$post = Timber::query_post();
$context['post'] = $post;
Timber::render( 'page-form.twig', $context );
