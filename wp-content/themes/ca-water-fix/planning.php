<?php

$context = Timber::get_context();
$template = 'planning-process.twig';
$context['title'] = 'Planning Process';
$context['all'] = $params['cat'];
if(!isset($params['cat'])){
	$eir = [
		'post_type' => 'resources',
		'order' => 'DESC',
		'order_by' => 'date',
		'resource_type' => 'eir-eis',
		'posts_per_page' => 3
	];

	$permits = [
		'post_type' => 'resources',
		'order' => 'DESC',
		'order_by' => 'date',
		'resource_type' => 'permits',
		'posts_per_page' => 3
	];

	$validation = [
		'post_type' => 'resources',
		'order' => 'DESC',
		'order_by' => 'date',
		'resource_type' => 'validation',
		'posts_per_page' => 3
	];
	$context['eir'] = Timber::query_posts($eir);
	$context['permits'] = Timber::get_posts($permits);
	$context['validation'] = Timber::get_posts($validation);
} else {
	$args = [
		'post_type' => 'resources',
		'order' => 'DESC',
		'order_by' => 'date',
		'posts_per_page' => -1,
		'tax_query' => array(
			array(
				'taxonomy' => 'resource_type',
				'field'    => 'slug',
				'terms'    => $params['cat'],
			),
		),
	];

	$context[($params['cat'] == 'eir-eis') ? 'eir' : $params['cat']] = Timber::get_posts($args);

}



//var_dump($context['all']);
Timber::render($template, $context);