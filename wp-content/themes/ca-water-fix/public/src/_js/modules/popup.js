function popUpOpen(el) {
    document.body.classList.add('popup-open');
    el.classList.add('open');
}

function setCookie(){
    //const expire = moment(new Date()).add(expiration, 'minutes').format("ddd, MMMM D YYYY, HH:mm:ss zz");
    const now = new Date();
    let time = now.getTime();
    time += 3600 * 1000;
    now.setTime(time);
    document.cookie = `new=true;expires=${now};max-age=3600;path=/`;
}

function readCookie() {
    return document.cookie.replace(/(?:(?:^|.*;\s*)new\s*\=\s*([^;]*).*$)|^.*$/, "$1");
}

function pagePop(element) {
    const cook = readCookie();
    if (cook !== 'true'){
        popUpOpen(element);
        setCookie();
    } else {

    }
}

export default pagePop;