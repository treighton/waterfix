/**
 * Adds functionality for fixing a navigation bar to the top of the screen on scroll
 * */

function fixNav(nav) {
    let topOfNav = nav.offsetTop;
    console.log(topOfNav)
    if(window.scrollY >= topOfNav) {
        document.body.style.paddingTop = nav.offsetHeight + 'px';
        nav.classList.add('fixedNav');
        console.log(topOfNav)
    } else {
        nav.classList.remove('fixedNav');
        document.body.style.paddingTop = 0;
    }
}

export default fixNav;