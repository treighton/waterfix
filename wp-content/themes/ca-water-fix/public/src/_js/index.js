import pagePop from './modules/popup'
import  fixedNav  from './modules/fixedNav'

const pop = document.querySelector('#pop');
const anchorNav = document.querySelector('.on-page-nav');

if (pop){
    setTimeout(
        function () {
            pagePop(pop, 2);
        },
        3000
    );
    const close = pop.querySelector('.close');

    close.addEventListener('click', () => {
        pop.classList.remove('open');
        document.body.classList.remove('popup-open')
    });


    window.addEventListener('scroll', () => {
        fixedNav(anchorNav)
    });
}



