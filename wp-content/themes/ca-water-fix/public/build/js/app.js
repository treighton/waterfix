/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/build/js/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 2);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
/**
 * Adds functionality for fixing a navigation bar to the top of the screen on scroll
 * */

function fixNav(nav) {
    var topOfNav = nav.offsetTop;
    console.log(topOfNav);
    if (window.scrollY >= topOfNav) {
        document.body.style.paddingTop = nav.offsetHeight + 'px';
        nav.classList.add('fixedNav');
        console.log(topOfNav);
    } else {
        nav.classList.remove('fixedNav');
        document.body.style.paddingTop = 0;
    }
}

exports.default = fixNav;

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
function popUpOpen(el) {
    document.body.classList.add('popup-open');
    el.classList.add('open');
}

function setCookie() {
    //const expire = moment(new Date()).add(expiration, 'minutes').format("ddd, MMMM D YYYY, HH:mm:ss zz");
    var now = new Date();
    var time = now.getTime();
    time += 3600 * 1000;
    now.setTime(time);
    document.cookie = 'new=true;expires=' + now + ';max-age=3600;path=/';
}

function readCookie() {
    return document.cookie.replace(/(?:(?:^|.*;\s*)new\s*\=\s*([^;]*).*$)|^.*$/, "$1");
}

function pagePop(element) {
    var cook = readCookie();
    if (cook !== 'true') {
        popUpOpen(element);
        setCookie();
    } else {}
}

exports.default = pagePop;

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _popup = __webpack_require__(1);

var _popup2 = _interopRequireDefault(_popup);

var _fixedNav = __webpack_require__(0);

var _fixedNav2 = _interopRequireDefault(_fixedNav);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var pop = document.querySelector('#pop');
var anchorNav = document.querySelector('.on-page-nav');

if (pop) {
    setTimeout(function () {
        (0, _popup2.default)(pop, 2);
    }, 3000);
    var close = pop.querySelector('.close');

    close.addEventListener('click', function () {
        pop.classList.remove('open');
        document.body.classList.remove('popup-open');
    });

    window.addEventListener('scroll', function () {
        (0, _fixedNav2.default)(anchorNav);
    });
}

/***/ })
/******/ ]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgZjU5M2JlMjgxZWU1ZDE3ODk0YzkiLCJ3ZWJwYWNrOi8vLy4vc3JjL19qcy9tb2R1bGVzL2ZpeGVkTmF2LmpzIiwid2VicGFjazovLy8uL3NyYy9fanMvbW9kdWxlcy9wb3B1cC5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvX2pzL2luZGV4LmpzIl0sIm5hbWVzIjpbImZpeE5hdiIsIm5hdiIsInRvcE9mTmF2Iiwib2Zmc2V0VG9wIiwiY29uc29sZSIsImxvZyIsIndpbmRvdyIsInNjcm9sbFkiLCJkb2N1bWVudCIsImJvZHkiLCJzdHlsZSIsInBhZGRpbmdUb3AiLCJvZmZzZXRIZWlnaHQiLCJjbGFzc0xpc3QiLCJhZGQiLCJyZW1vdmUiLCJwb3BVcE9wZW4iLCJlbCIsInNldENvb2tpZSIsIm5vdyIsIkRhdGUiLCJ0aW1lIiwiZ2V0VGltZSIsInNldFRpbWUiLCJjb29raWUiLCJyZWFkQ29va2llIiwicmVwbGFjZSIsInBhZ2VQb3AiLCJlbGVtZW50IiwiY29vayIsInBvcCIsInF1ZXJ5U2VsZWN0b3IiLCJhbmNob3JOYXYiLCJzZXRUaW1lb3V0IiwiY2xvc2UiLCJhZGRFdmVudExpc3RlbmVyIl0sIm1hcHBpbmdzIjoiO0FBQUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0EsbURBQTJDLGNBQWM7O0FBRXpEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsbUNBQTJCLDBCQUEwQixFQUFFO0FBQ3ZELHlDQUFpQyxlQUFlO0FBQ2hEO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDhEQUFzRCwrREFBK0Q7O0FBRXJIO0FBQ0E7O0FBRUE7QUFDQTs7Ozs7Ozs7Ozs7OztBQ2hFQTs7OztBQUlBLFNBQVNBLE1BQVQsQ0FBZ0JDLEdBQWhCLEVBQXFCO0FBQ2pCLFFBQUlDLFdBQVdELElBQUlFLFNBQW5CO0FBQ0FDLFlBQVFDLEdBQVIsQ0FBWUgsUUFBWjtBQUNBLFFBQUdJLE9BQU9DLE9BQVAsSUFBa0JMLFFBQXJCLEVBQStCO0FBQzNCTSxpQkFBU0MsSUFBVCxDQUFjQyxLQUFkLENBQW9CQyxVQUFwQixHQUFpQ1YsSUFBSVcsWUFBSixHQUFtQixJQUFwRDtBQUNBWCxZQUFJWSxTQUFKLENBQWNDLEdBQWQsQ0FBa0IsVUFBbEI7QUFDQVYsZ0JBQVFDLEdBQVIsQ0FBWUgsUUFBWjtBQUNILEtBSkQsTUFJTztBQUNIRCxZQUFJWSxTQUFKLENBQWNFLE1BQWQsQ0FBcUIsVUFBckI7QUFDQVAsaUJBQVNDLElBQVQsQ0FBY0MsS0FBZCxDQUFvQkMsVUFBcEIsR0FBaUMsQ0FBakM7QUFDSDtBQUNKOztrQkFFY1gsTTs7Ozs7Ozs7Ozs7O0FDakJmLFNBQVNnQixTQUFULENBQW1CQyxFQUFuQixFQUF1QjtBQUNuQlQsYUFBU0MsSUFBVCxDQUFjSSxTQUFkLENBQXdCQyxHQUF4QixDQUE0QixZQUE1QjtBQUNBRyxPQUFHSixTQUFILENBQWFDLEdBQWIsQ0FBaUIsTUFBakI7QUFDSDs7QUFFRCxTQUFTSSxTQUFULEdBQW9CO0FBQ2hCO0FBQ0EsUUFBTUMsTUFBTSxJQUFJQyxJQUFKLEVBQVo7QUFDQSxRQUFJQyxPQUFPRixJQUFJRyxPQUFKLEVBQVg7QUFDQUQsWUFBUSxPQUFPLElBQWY7QUFDQUYsUUFBSUksT0FBSixDQUFZRixJQUFaO0FBQ0FiLGFBQVNnQixNQUFULHlCQUFzQ0wsR0FBdEM7QUFDSDs7QUFFRCxTQUFTTSxVQUFULEdBQXNCO0FBQ2xCLFdBQU9qQixTQUFTZ0IsTUFBVCxDQUFnQkUsT0FBaEIsQ0FBd0IsNENBQXhCLEVBQXNFLElBQXRFLENBQVA7QUFDSDs7QUFFRCxTQUFTQyxPQUFULENBQWlCQyxPQUFqQixFQUEwQjtBQUN0QixRQUFNQyxPQUFPSixZQUFiO0FBQ0EsUUFBSUksU0FBUyxNQUFiLEVBQW9CO0FBQ2hCYixrQkFBVVksT0FBVjtBQUNBVjtBQUNILEtBSEQsTUFHTyxDQUVOO0FBQ0o7O2tCQUVjUyxPOzs7Ozs7Ozs7QUM1QmY7Ozs7QUFDQTs7Ozs7O0FBRUEsSUFBTUcsTUFBTXRCLFNBQVN1QixhQUFULENBQXVCLE1BQXZCLENBQVo7QUFDQSxJQUFNQyxZQUFZeEIsU0FBU3VCLGFBQVQsQ0FBdUIsY0FBdkIsQ0FBbEI7O0FBRUEsSUFBSUQsR0FBSixFQUFRO0FBQ0pHLGVBQ0ksWUFBWTtBQUNSLDZCQUFRSCxHQUFSLEVBQWEsQ0FBYjtBQUNILEtBSEwsRUFJSSxJQUpKO0FBTUEsUUFBTUksUUFBUUosSUFBSUMsYUFBSixDQUFrQixRQUFsQixDQUFkOztBQUVBRyxVQUFNQyxnQkFBTixDQUF1QixPQUF2QixFQUFnQyxZQUFNO0FBQ2xDTCxZQUFJakIsU0FBSixDQUFjRSxNQUFkLENBQXFCLE1BQXJCO0FBQ0FQLGlCQUFTQyxJQUFULENBQWNJLFNBQWQsQ0FBd0JFLE1BQXhCLENBQStCLFlBQS9CO0FBQ0gsS0FIRDs7QUFNQVQsV0FBTzZCLGdCQUFQLENBQXdCLFFBQXhCLEVBQWtDLFlBQU07QUFDcEMsZ0NBQVNILFNBQVQ7QUFDSCxLQUZEO0FBR0gsQyIsImZpbGUiOiJhcHAuanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSlcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcblxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gaWRlbnRpdHkgZnVuY3Rpb24gZm9yIGNhbGxpbmcgaGFybW9ueSBpbXBvcnRzIHdpdGggdGhlIGNvcnJlY3QgY29udGV4dFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5pID0gZnVuY3Rpb24odmFsdWUpIHsgcmV0dXJuIHZhbHVlOyB9O1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHtcbiBcdFx0XHRcdGNvbmZpZ3VyYWJsZTogZmFsc2UsXG4gXHRcdFx0XHRlbnVtZXJhYmxlOiB0cnVlLFxuIFx0XHRcdFx0Z2V0OiBnZXR0ZXJcbiBcdFx0XHR9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCIvYnVpbGQvanMvXCI7XG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gMik7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gd2VicGFjay9ib290c3RyYXAgZjU5M2JlMjgxZWU1ZDE3ODk0YzkiLCIvKipcbiAqIEFkZHMgZnVuY3Rpb25hbGl0eSBmb3IgZml4aW5nIGEgbmF2aWdhdGlvbiBiYXIgdG8gdGhlIHRvcCBvZiB0aGUgc2NyZWVuIG9uIHNjcm9sbFxuICogKi9cblxuZnVuY3Rpb24gZml4TmF2KG5hdikge1xuICAgIGxldCB0b3BPZk5hdiA9IG5hdi5vZmZzZXRUb3A7XG4gICAgY29uc29sZS5sb2codG9wT2ZOYXYpXG4gICAgaWYod2luZG93LnNjcm9sbFkgPj0gdG9wT2ZOYXYpIHtcbiAgICAgICAgZG9jdW1lbnQuYm9keS5zdHlsZS5wYWRkaW5nVG9wID0gbmF2Lm9mZnNldEhlaWdodCArICdweCc7XG4gICAgICAgIG5hdi5jbGFzc0xpc3QuYWRkKCdmaXhlZE5hdicpO1xuICAgICAgICBjb25zb2xlLmxvZyh0b3BPZk5hdilcbiAgICB9IGVsc2Uge1xuICAgICAgICBuYXYuY2xhc3NMaXN0LnJlbW92ZSgnZml4ZWROYXYnKTtcbiAgICAgICAgZG9jdW1lbnQuYm9keS5zdHlsZS5wYWRkaW5nVG9wID0gMDtcbiAgICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IGZpeE5hdjtcblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvX2pzL21vZHVsZXMvZml4ZWROYXYuanMiLCJmdW5jdGlvbiBwb3BVcE9wZW4oZWwpIHtcbiAgICBkb2N1bWVudC5ib2R5LmNsYXNzTGlzdC5hZGQoJ3BvcHVwLW9wZW4nKTtcbiAgICBlbC5jbGFzc0xpc3QuYWRkKCdvcGVuJyk7XG59XG5cbmZ1bmN0aW9uIHNldENvb2tpZSgpe1xuICAgIC8vY29uc3QgZXhwaXJlID0gbW9tZW50KG5ldyBEYXRlKCkpLmFkZChleHBpcmF0aW9uLCAnbWludXRlcycpLmZvcm1hdChcImRkZCwgTU1NTSBEIFlZWVksIEhIOm1tOnNzIHp6XCIpO1xuICAgIGNvbnN0IG5vdyA9IG5ldyBEYXRlKCk7XG4gICAgbGV0IHRpbWUgPSBub3cuZ2V0VGltZSgpO1xuICAgIHRpbWUgKz0gMzYwMCAqIDEwMDA7XG4gICAgbm93LnNldFRpbWUodGltZSk7XG4gICAgZG9jdW1lbnQuY29va2llID0gYG5ldz10cnVlO2V4cGlyZXM9JHtub3d9O21heC1hZ2U9MzYwMDtwYXRoPS9gO1xufVxuXG5mdW5jdGlvbiByZWFkQ29va2llKCkge1xuICAgIHJldHVybiBkb2N1bWVudC5jb29raWUucmVwbGFjZSgvKD86KD86XnwuKjtcXHMqKW5ld1xccypcXD1cXHMqKFteO10qKS4qJCl8Xi4qJC8sIFwiJDFcIik7XG59XG5cbmZ1bmN0aW9uIHBhZ2VQb3AoZWxlbWVudCkge1xuICAgIGNvbnN0IGNvb2sgPSByZWFkQ29va2llKCk7XG4gICAgaWYgKGNvb2sgIT09ICd0cnVlJyl7XG4gICAgICAgIHBvcFVwT3BlbihlbGVtZW50KTtcbiAgICAgICAgc2V0Q29va2llKCk7XG4gICAgfSBlbHNlIHtcblxuICAgIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgcGFnZVBvcDtcblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvX2pzL21vZHVsZXMvcG9wdXAuanMiLCJpbXBvcnQgcGFnZVBvcCBmcm9tICcuL21vZHVsZXMvcG9wdXAnXG5pbXBvcnQgIGZpeGVkTmF2ICBmcm9tICcuL21vZHVsZXMvZml4ZWROYXYnXG5cbmNvbnN0IHBvcCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJyNwb3AnKTtcbmNvbnN0IGFuY2hvck5hdiA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5vbi1wYWdlLW5hdicpO1xuXG5pZiAocG9wKXtcbiAgICBzZXRUaW1lb3V0KFxuICAgICAgICBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICBwYWdlUG9wKHBvcCwgMik7XG4gICAgICAgIH0sXG4gICAgICAgIDMwMDBcbiAgICApO1xuICAgIGNvbnN0IGNsb3NlID0gcG9wLnF1ZXJ5U2VsZWN0b3IoJy5jbG9zZScpO1xuXG4gICAgY2xvc2UuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCAoKSA9PiB7XG4gICAgICAgIHBvcC5jbGFzc0xpc3QucmVtb3ZlKCdvcGVuJyk7XG4gICAgICAgIGRvY3VtZW50LmJvZHkuY2xhc3NMaXN0LnJlbW92ZSgncG9wdXAtb3BlbicpXG4gICAgfSk7XG5cblxuICAgIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKCdzY3JvbGwnLCAoKSA9PiB7XG4gICAgICAgIGZpeGVkTmF2KGFuY2hvck5hdilcbiAgICB9KTtcbn1cblxuXG5cblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9fanMvaW5kZXguanMiXSwic291cmNlUm9vdCI6IiJ9