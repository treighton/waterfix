<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticIniteb8a8a847981c931c82ca937fd01ad22
{
    public static $prefixLengthsPsr4 = array (
        'D' => 
        array (
            'DrewM\\MailChimp\\' => 16,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'DrewM\\MailChimp\\' => 
        array (
            0 => __DIR__ . '/..' . '/drewm/mailchimp-api/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticIniteb8a8a847981c931c82ca937fd01ad22::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticIniteb8a8a847981c931c82ca937fd01ad22::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
