<?php

/**
 * Fired during plugin activation
 *
 * @link       pagedesigngroup.com
 * @since      1.0.0
 *
 * @package    Buletin
 * @subpackage Buletin/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Buletin
 * @subpackage Buletin/includes
 * @author     Treighton + PDG <treighton@pagedesigngroup.com>
 */
class Buletin_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
