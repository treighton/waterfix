<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       pagedesigngroup.com
 * @since      1.0.0
 *
 * @package    Buletin
 * @subpackage Buletin/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Buletin
 * @subpackage Buletin/admin
 * @author     Treighton + PDG <treighton@pagedesigngroup.com>
 */
class Buletin_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * @var stdClass in
	 */

	private $digest;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		wp_enqueue_style(
			$this->plugin_name,
			plugin_dir_url( __FILE__ ) . 'css/buletin-admin.css',
			array(),
			$this->version,
			'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		wp_enqueue_script(
			$this->plugin_name,
			plugin_dir_url( __FILE__ ) . 'js/buletin-admin.js', array( 'jquery' ),
			$this->version,
			false
		);

	}

	/**
	 * Register admin pages
	 *
	 * @since 1.0.0
	 */

	public function register_settings_pages(){

		add_menu_page(
			'Buletin',
			'Buletin',
			'manage_options',
			'buletin-options'
		);
		add_submenu_page(
			'buletin-options',
			'Buletin Options',
			'Buletin Options',
			'manage_options',
			'buletin-options',
			array($this, 'add_buletin_settings_page')
		);
		add_submenu_page(
			'buletin-options',
			'Digests',
			'Digests',
			'manage_options',
			'buletin-digests',
			array($this, 'add_buletin_digest_page')
		);

	}

	/**
	 * Register the settings for our settings page.
	 *
	 * @since    1.0.0
	 */

	public function register_settings() {

		register_setting(
			$this->plugin_name . '-settings',
			$this->plugin_name . '-settings',
			array( $this, 'sandbox_register_setting' )
		);

		add_settings_section(
			$this->plugin_name . '-settings-section',
			__( 'Settings', 'buletin' ),
			array( $this, 'sandbox_add_settings_section' ),
			$this->plugin_name . '-settings'
		);

		add_settings_field(
			'api-key',
			__( 'Api Key', 'buletin' ),
			array( $this, 'sandbox_add_settings_field_input_text' ),
			$this->plugin_name . '-settings',
			$this->plugin_name . '-settings-section',
			array(
				'label_for' => 'api-key',
				'default'   => __( 'Enter Api key from Mail Chimp', 'buletin' )
			)
		);

		add_settings_field(
			'mc-list',
			__( 'MC List ID', 'buletin' ),
			array( $this, 'sandbox_add_settings_field_input_text' ),
			$this->plugin_name . '-settings',
			$this->plugin_name . '-settings-section',
			array(
				'label_for' => 'mc-list',
				'default'   => __( 'Enter List ID from Mail Chimp', 'buletin' )
			)
		);

		add_settings_field(
			'tax-slug',
			__( 'Taxonomy to Track', 'buletin' ),
			array( $this, 'sandbox_add_settings_field_input_text' ),
			$this->plugin_name . '-settings',
			$this->plugin_name . '-settings-section',
			array(
				'label_for' => 'tax-slug',
				'default'   => __( 'Enter the slug of the taxonomy you want to tack', 'buletin' )
			)
		);

	}

	/**
	 * Sandbox our settings.
	 *
	 * @since    1.0.0
	 */
	public function sandbox_register_setting( $input ) {

		$new_input = array();

		if ( isset( $input ) ) {
			// Loop trough each input and sanitize the value if the input id isn't post-types
			foreach ( $input as $key => $value ) {
				if ( $key == 'post-types' ) {
					$new_input[ $key ] = $value;
				} else {
					$new_input[ $key ] = sanitize_text_field( $value );
				}
			}
		}

		return $new_input;

	}

	/**
	 * Sandbox our section for the settings.
	 *
	 * @since    1.0.0
	 */
	public function sandbox_add_settings_section() {

		return;

	}

	/**
	 * Sandbox our inputs with text
	 *
	 * @since    1.0.0
	 */
	public function sandbox_add_settings_field_input_text( $args ) {
		$field_id = $args['label_for'];
		$field_default = $args['default'];
		$options = get_option( $this->plugin_name . '-settings' );
		$option = $field_default;
		if ( ! empty( $options[ $field_id ] ) ) {
			$option = $options[ $field_id ];
		}
		?>

		<input type="text" name="<?php echo $this->plugin_name . '-settings[' . $field_id . ']'; ?>" id="<?php echo $this->plugin_name . '-settings[' . $field_id . ']'; ?>" value="<?php echo esc_attr( $option ); ?>" class="regular-text" />

		<?php
	}

	/**
	 * Render Main Options Page
	 *
	 * @since
	 */

	public function add_buletin_settings_page() {

		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/buletin-admin-display.php';

	}

	/**
	 * Render Main functionality Page
	 * lists digests
	 * shows status of digest ( no campaign, pending, sent w/ date )
	 *
	 * @since
	 */

	public function add_buletin_digest_page() {

		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/buletin-admin-display-digests.php';

	}
}
