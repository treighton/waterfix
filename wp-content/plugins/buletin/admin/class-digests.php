<?php
/**
 * This is a factory for settings fields
 *
 *
 * @link       pagedesigngroup.com
 * @since      1.0.0
 *
 * @package    Buletin
 * @subpackage Buletin/admin/includes
 */

include('../vendor/drewm/mailchimp-api/src/MailChimp.php');

class Digests {

	protected $mailchimp;

	/**
	 *  Get Issues
	 *
	 * @param string $taxonomy Taxonomy to query
	 *
	 * @return array $issues All issues in query
	 *
	 * @since 1.0.0
	 */

	public function get_issues($taxonomy){
		$issues = get_terms($taxonomy);
		return $issues;
	}

	/**
	 * Get Issue Status
	 *
	 * @param int $id Issue ID
	 *
	 * @return mixed $status either string 'unpublished' or int mailchimp list ID based on if a list exists on MC
	 *
	 * @since 1.0.0
	 */

	public function get_issue_status($id){
		$status = get_term_meta($id,'list_status', true);
		return $status;
	}

	/**
	 * Get Issue Post Count
	 *
	 * @param int $id ID of the taxonomy to get post count for
	 *
	 * @return int $count Number of posts associated with the queried issue
	 *
	 * @since 1.0.0
	 */

	public function get_issues_post_count($id) {
		$posts = get_posts(array(
			'fields' =>'ids',
			'posts_per_page' => -1,
			'tax_query' => [
				[
					'taxonomy' => 'issue',
					'field'    => 'term_id',
					'terms'    => $id,
				]
			]
		));

		return sizeof($posts);

	}

	/**
	 * Render Button based on List status
	 *
	 * @param mixed $status string if no list on MC int if the list exists on MC
	 *
	 * @return string html string for the button to be rendered
	 *
	 */

	public function render_action_button($status){

	}

	/**
	 * Render Issues
	 *
	 * @since 1.0.0
	 */

	public function render_issues(){

	}

	/**
	 *  Create Mailchimp Campaign
	 *
	 * @param array $args settings as required by MC API to create a campaign
	 *
	 * @return int $id Mailchimp List ID
	 */

	public function create_campaign($args){

	}

	/**
	 * Build Template for MC
	 *
	 * @param mixed $posts
	 *
	 * @return string $html String of HTML for campaign
	 */

	public function build_template($posts){

	}

	/**
	 * Get Issue Posts
	 *
	 * @param int $id ID of Issue to query posts
	 *
	 * @return mixed $posts
	 */

	public function get_issue_posts(){

	}

	/**
	 * Update Campaign pushes changes from WP to MC
	 *
	 * @param array $args things to change / add to MC campaign
	 *
	 */

	public function update_campaign(){

	}

	/**
	 * Update Issue Status
	 *
	 * @param int $id ID of issue to update
	 *
	 * @param string|int $status unpublish or ID of MC Campaign
	 *
	 */

	public function update_issue_status() {

	}
}