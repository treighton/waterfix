<?php
/**
 * Plugin Name:     Jay's on import
 * Description:     Import Posts from Json
 * Author:          Page Design GRoup
 * Text Domain:     jsonimport
 * Domain Path:     /languages
 * Version:         0.1.0
 *
 * @package         JsonImport
 */

add_action( 'wp_loaded', array ( json_post_loader::get_instance(), 'register' ) );

class json_post_loader {
	protected static $instance = NULL;

	protected $action     = 'json_post_loader';
	protected $option_name     = 'json_post_loader';
	protected $page_id = NULL;

	/**
	 * Access this plugin’s working instance
	 *
	 * @wp-hook wp_loaded
	 * @return  object of this class
	 */
	public static function get_instance() {
		NULL === self::$instance and self::$instance = new self;
		return self::$instance;
	}

	public function register() {
		add_action( 'admin_menu', array ( $this, 'add_menu' ) );
		add_action( "admin_post_$this->action", array ( $this, 'admin_post' ) );

	}

	public function add_menu() {
		$page_id = add_options_page(
			'Load Posts',
			'Load Posts',
			'manage_options',
			'load-posts',
			array ( $this, 'render_options_page' )
		);

		add_action( "load-$page_id", array ( $this, 'parse_message' ) );
	}

	public function parse_message() {
		if ( ! isset ( $_GET['msg'] ) )
			return;

		$text = FALSE;

		if ( 'updated' === $_GET['msg'] )
			$this->load_articles();
			$this->msg_text = 'Posts Loaded';

		if ( 'deleted' === $_GET['msg'] )
			$this->msg_text = 'Posts not loaded!';

		if ( $this->msg_text )
			add_action( 'admin_notices', array ( $this, 'render_msg' ) );
	}

	public function render_msg() {
		echo '<div class="' . esc_attr( $_GET['msg'] ) . '"><p>'
		     . $this->msg_text . '</p></div>';
	}


	public function render_options_page() {
		$option = esc_attr( stripslashes( get_option( $this->option_name ) ) );
		$redirect = urlencode( remove_query_arg( 'msg', $_SERVER['REQUEST_URI'] ) );
		$redirect = urlencode( $_SERVER['REQUEST_URI'] );
		$disable = false

		?><h1><?php echo $GLOBALS['title']; ?></h1>
		<form action="<?php echo admin_url( 'admin-post.php' ); ?>" method="POST">
			<input type="hidden" name="action" value="<?php echo $this->action; ?>">
			<?php wp_nonce_field( $this->action, $this->option_name . '_nonce', FALSE ); ?>
			<input type="hidden" name="_wp_http_referer" value="<?php echo $redirect; ?>">
			<?php
			if ($option == 'load'){
				$disable = true;
				$label = 'posts have already been imported ';
			} else {
				$label = 'enter load to load posts';
			}
			?>
			<label for="<?php echo $this->option_name; ?>"><?php echo $label ?></label>

			<input type="text" <?php if ($option == 'load'): echo 'disabled'; endif; ?> name="<?php echo $this->option_name; ?>" id="<?php echo $this->option_name; ?>" value="<?php echo $option; ?>">
			<?php
			if ($disable){
				submit_button( 'Send', '','', '', array('disabled' => $disable) );
			} else {
				submit_button( 'Send' );
			}
			 ?>
		</form>
		<?php
	}

	public function load_articles() {
		$dir =__DIR__.'/json-rewrite/';
		if(is_dir($dir)) {
			if($dh = opendir($dir)){
				while (($file = readdir($dh)) !== false){
					if($file != "." && $file != "..") {
						$postsToImport = file_get_contents($dir.$file);
						$this->add_articles($postsToImport);
					}
				}
			}
		}

	}

	public function add_articles($json) {

		$newPosts = json_decode($json, true);

		foreach($newPosts as $article) {

			$post =  array(
				'post_type'     => 'resources',
				'post_title'    => $article["title"],
				'post_content'  =>  $article["content"],
				'post_status'   => 'publish',
				'post_date'		=>  $article["date"],
			);

			$post_id = wp_insert_post($post);

			//var_dump($post);

			if ( $post_id ) {
				update_post_meta( $post_id, 'url', $article["link"] );
				update_post_meta( $post_id, 'subtitle', $article["subtitle"] );
				wp_set_object_terms( $post_id, $article["cat"], 'resource_type' );
			}
			//echo "<p>Celebration Time? Looks like <strong>".$article["title"]."</strong> has been added</p>";
		}

	}

	public function admin_post() {
		if ( ! wp_verify_nonce( $_POST[ $this->option_name . '_nonce' ], $this->action ) ) {
			die( 'Invalid nonce.' . var_export( $_POST, true ) );
		}

		if ( isset ( $_POST[ $this->option_name ] ) && $_POST[ $this->option_name ] == 'load') {
			update_option( $this->option_name, $_POST[ $this->option_name ] );
			$msg = 'updated';
		} else {
			delete_option( $this->option_name );
			$msg = 'deleted';
		}

		if ( ! isset ( $_POST['_wp_http_referer'] ) ) {
			die( 'Missing target.' );
		}

		$url = add_query_arg( 'msg', $msg, urldecode( $_POST['_wp_http_referer'] ) );

		wp_safe_redirect( $url );
		exit;
	}
}