<?php
/**
 * Plugin Name:     Cawf
 * Description:     Custom functionality for the CA waterfix website
 * Author:          Page Design GRoup
 * Text Domain:     cawf
 * Domain Path:     /languages
 * Version:         0.1.0
 *
 * @package         Cawf
 */

if (!defined('WPINC')) {
    die();
}

include __DIR__ . '/includes/cpts.php';

$resources = new Custom_Post_Type('resources');
$resources->add_taxonomy('Resource Type');

$resources = new Custom_Post_Type('Monthly Update');

//array( 'rewrite' => array('slug' => 'newsletters/%newsletterscategory%', 'with_front' => false)

$news_letters = new Custom_Post_Type('Newsletters', array(
    'supports' => array('editor', 'title', 'thumbnail', 'excerpt')
));

$news_letters->add_taxonomy('Issue');

function generate_taxonomy_rewrite_rules($wp_rewrite)
{
    $rules = array();
    $post_types = get_post_types(
        array('name' => 'resources', 'public' => true, '_builtin' => false),
        'objects'
    );
    $taxonomies = get_taxonomies(
        array('name' => 'resource_type', 'public' => true, '_builtin' => false),
        'objects'
    );

    foreach ($post_types as $post_type) {
        $post_type_name = $post_type->name;
        $post_type_slug = $post_type->rewrite['slug'];

        foreach ($taxonomies as $taxonomy) {
            if ($taxonomy->object_type[0] == $post_type_name) {
                $terms = get_categories(array(
                    'type' => $post_type_name,
                    'taxonomy' => $taxonomy->name,
                    'hide_empty' => 0
                ));
                foreach ($terms as $term) {
                    $rules[$post_type_slug . '/' . $term->slug . '/?$'] =
                        'index.php?' . $term->taxonomy . '=' . $term->slug;
                }
            }
        }
    }
    $wp_rewrite->rules = $rules + $wp_rewrite->rules;
}
add_action('generate_rewrite_rules', 'generate_taxonomy_rewrite_rules');

if (function_exists('acf_add_local_field_group')):
    acf_add_local_field_group(array(
        'key' => 'group_5b760af44c580',
        'title' => 'newsletter',
        'fields' => array(
            array(
                'key' => 'field_5b760b01dbebf',
                'label' => 'Issue Intro',
                'name' => 'issue_intro',
                'type' => 'wysiwyg',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => ''
                ),
                'default_value' => '',
                'tabs' => 'all',
                'toolbar' => 'full',
                'media_upload' => 1,
                'delay' => 0
            )
        ),
        'location' => array(
            array(
                array(
                    'param' => 'taxonomy',
                    'operator' => '==',
                    'value' => 'issue'
                )
            )
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => 1,
        'description' => ''
    ));
    acf_add_local_field_group(array(
        'key' => 'group_5c0988229e184',
        'title' => 'Monthly Updates',
        'fields' => array(
            array(
                'key' => 'field_5c09882e470ae',
                'label' => 'Monthly Update Download',
                'name' => 'monthly_update_download',
                'type' => 'file',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => ''
                ),
                'return_format' => 'url',
                'library' => 'all',
                'min_size' => '',
                'max_size' => '',
                'mime_types' => ''
            )
        ),
        'location' => array(
            array(
                array(
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'monthly_update'
                )
            )
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => 1,
        'description' => ''
    ));
endif;

function wpse_287931_register_categories_names_field()
{
    register_rest_field('resources', 'resrource_type_title', array(
        'get_callback' => 'wpse_287931_get_categories_names',
        'update_callback' => null,
        'schema' => null
    ));
}

add_action('rest_api_init', 'wpse_287931_register_categories_names_field');

function wpse_287931_get_categories_names($object, $field_name, $request)
{
    $formatted_categories = array();

    $categories = get_term($object['resource_type'], 'resource_type');

    foreach ($object['resource_type'] as $category) {
        $cat = get_term($category, 'resource_type');
        $formatted_categories[] = $cat->name;
    }

    return $formatted_categories;
}

add_action('transition_post_status', 'send_new_post', 10, 3);

// Listen for publishing of a new post
function send_new_post($new_status, $old_status, $post)
{
    if (
        $post->post_type === 'resources' ||
        $post->post_type === 'monthly_update' ||
        $post->post_type === 'newsletters'
    ) {
        wp_remote_post(
            'https: //api.netlify.com/build_hooks/5b9e04904ed62f625d3cf95a'
        );
        wp_remote_post(
            'https://api.netlify.com/build_hooks/5bef3d02c96592526fbd73c1'
        );
    }
}
