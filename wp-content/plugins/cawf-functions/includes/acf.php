<?php
/**
 * Created by PhpStorm.
 * User: treighton
 * Date: 1/9/18
 * Time: 2:26 PM
 */

include_once(__DIR__.'/acf/acf.php');

class register_acf_fields {

	private $fields;

	public function __construct($args) {

		$this->fields = $args;

		add_action('acf/init', array($this, 'add_local_field_groups'));

	}

	public function add_local_field_groups() {

		acf_add_local_field_group($this->fields);

	}

}