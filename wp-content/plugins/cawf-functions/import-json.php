<?php


$json = file_get_contents(get_template_directory_uri().'/wellspring.json');

add_articles($json);

function add_articles($json) {

	$articles = json_decode($json, true);

	foreach ($articles["items"] as $article) {

		$post = array(
			'post_title'    => $article["title"],
			'post_content'  => $article["content_html"],
			'post_type'     => 'post',
			'post_status'   => 'publish',
			'post_date'		=> $article["date_published"],
		);

		$post_id = wp_insert_post( $post );

		if ( $post_id ) {
			//update_post_meta( $post_id, 'issue', $article["_customfields"]["issuedate"] );
			update_post_meta( $post_id, 'articleheader', $article["_customfields"]["articleheader"] );
			wp_set_object_terms( $post_id, $article["tags"], 'category' );
			wp_set_object_terms( $post_id, $article["_customfields"]["issuedate"], "issue" );

			if ($article["comments"]) {
				foreach ($article["comments"] as $comment) {
					$comment_data = array(
						'comment_post_ID' => $post_id,
						'comment_author' => $comment["comment_author"],
						'comment_content' => $comment["comment_content"],
						'comment_date' => $comment["comment_date"],
						'comment_date_gmt' => $comment["comment_date_gmt"],
						'comment_approved' => 1,
					);
					$comment_id = wp_insert_comment($comment_data);
				}
			}
			echo "<p>Celebration Time? Looks like <strong>".$article["title"]."</strong> has been added</p>";

		}

	}

}


?>
